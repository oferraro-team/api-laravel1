<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::prefix('api')->group(function () {
    Route::get('/questionnaire', 'QuestionnaireController@index')->name('questionnaire');
    Route::get('/questionnaire/{id}/data', 'QuestionnaireController@data')->name('questionnaire.data');
    Route::post('/questionnaire/add', 'QuestionnaireController@add')->name('questionnaire.add');
    Route::post('/question/add', 'QuestionController@add')->name('question.add');
    Route::get('/answer/{id}/children', 'AnswerController@children')->name('answer.children');
    Route::post('/answer/add', 'AnswerController@add')->name('answer.add');
    Route::get('/client-answers/{clientId}/{questionnaireId}', 'ClientAnswerController@clientAnswers')->name('client_answers');
    Route::post('/client-answer/add', 'ClientAnswerController@add')->name('client_answer.add');
});
