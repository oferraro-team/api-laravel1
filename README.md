Laravel App

- Create local database and user
i.e. (with mysql cli or any Mysql GUI)
    CREATE USER 'questionaire'@'localhost' IDENTIFIED BY 'j2k0c,a-';
    CREATE DATABASE mw_test;
    GRANT ALL PRIVILEGES ON  mw_test.* TO 'questionaire'@'localhost';
    FLUSH PRIVILEGES;

- Create .env (just copy .env.example and replace database values)

- Run migrations to have the tables (there are two tables which comes with the framework, users and failed_jobs)
php artisan migrate 


Notes: 
- Constrains could be a good idea for a future implementation in this database project
 (for the purpose of the test I think it's the same)

- Postman or curl can be used to reach the endpoints (endpoints are defined at routes/web.php)

- Allow CORS to access the API and remove CSRF token verification
 (API should have token / oauth or any other authentication)

- For the test, just assume the user_ids are valid

- Endpoints validation will allow to continue with the requests or show the main screen
 (Errors could be specify or not, depending on how much information we want to give to the user, mostly for security reasons)

- To see all the routes/endpoints use this in command line:
php artisan route:list

Todo: 
- Add tests for each possibility in each endpoint
- Add valid_between_dates field to questionnaire
- Add enabled (true/false) field to questionnaire
- Check if questionnaire is enabled 
- Use parameters to return only specific page questionnaire questions/answers
- Add client_id to client-answer/add callback
- Add endpoints to batch add (at least for questions and answers)

Endpoint test callbacks:
- get: /questionnaire
- get: /questionnaire/{id}/data
- get: /answer/{id}/children
- get: /client-answers/{clientId}/{questionnaireId}

- post: /questionnaire/add
    sample data:
    {
        "name": "test questionnaire",
        "user_id": 1
    }    
- post: /question/add
    sample data:
    {
        "questionnaire_id": "1",
        "page_number": "1",
        "order": "1",
        "type": "4",
        "question": "Test question",
        "required": 1
    }
- post: /answer/add
    sample data:
    {
        "question_id": 1,
        "order": 4,
        "parent_id": 0,
        "value": "test question1"
    }
    {    
        "question_id": 1,
        "order": 3,
        "parent_id": 1,
        "value": "test question children id 1"
    }
- post: /client-answer/add
    sample data:
    { 
        "answer_id": 1,
        "client_id": 1,
        "value": "test client answer"
    }
