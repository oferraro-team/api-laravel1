<?php

namespace App\Http\Controllers;

use App\answer;
use App\question;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    public function add(Request $request) {
        $request->validate([
            'question_id' => 'required|integer',
            'order' => 'required|integer',
            'parent_id' => 'required|integer'
        ]);
        $question = question::find($request->get('question_id'));
        if ($question) {
            if ((($question->type == question::TYPE_SLIDER
                || $question->type == question::TYPE_TEXT)
                && $request->get('value')) // Question type requires value (integer or string)
                || ($question->type == question::TYPE_SINGLE_SELECT
                    || $question->type == question::TYPE_MULTI_SELECT
                ) // Question type is a selector, value is not needed
            ) {
                $answer = new answer;
                $answer->question_id = $request->get('question_id');
	            $answer->order = $request->get('order');
	            $answer->parent_id = $request->get('parent_id');
	            $answer->value = $request->get('value');
                $answer->save();
                if ($answer) {
                    return response()->json([
                        'answer' => $answer,
                    ], 200);
                }
            } else {
                return $this->returnError('parameters error');
            }
        } else {
            return $this->returnError('parameters error');
        }
    }

    private function returnError($error, $errorNumber = 400) // TODO: add this function in a helper to use it everywhere
    {
        return response()->json([
            'error' => "$error",
        ], $errorNumber);
    }

    public function children($id) { // id parameter is the answer id
        $answer = answer::find($id);
        $question = question::find($answer->question_id);
        if ($answer && $question && $question->type == Question::TYPE_MULTI_SELECT) { // it's a multi select question type
            $answers = answer::where('parent_id', $answer->id)->get()->sortBy('order');
            return response()->json([
                'answers' => $answers,
            ], 200);
        } else {
            return response()->json([
                'error' => 'parameters error',
            ], 400); // Tell the user question doesn't exist or any other specific message? yes or not it's just about security
        }
    }
}
