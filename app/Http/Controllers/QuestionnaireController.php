<?php

namespace App\Http\Controllers;

use App\answer;
use App\question;
use App\questionnaire;
use Illuminate\Http\Request;

class QuestionnaireController extends Controller
{
    public function index() { // Return all the Questionnaires
        $questionnaires = Questionnaire::all();
        return response()->json([
            'questionnaires' => $questionnaires,
        ], 200);
    }

    public function add(Request $request) { // Add a Questionnaire
        $request->validate([
            'name' => 'required',
            'user_id' => 'required|integer',
        ]);
        $questionnaire = new questionnaire;
        $questionnaire->name = $request->get('name');
        $questionnaire->user_id = $request->get('user_id');
        if ($questionnaire->save()) {
            return response()->json([
                'questionnaire' => $questionnaire,
            ], 200);
        } // else show an error here? if so, how much specific
    }

    public function data($id) {
        $questionnaire = questionnaire::find($id);
        if ($questionnaire) {
            $questions = question::where('questionnaire_id', $questionnaire->id)->get()->sortBy('order');
            $answerQuestions = [];
            foreach($questions as $question) {
                $answers = answer::where('question_id', $question->id)->get()->sortBy('order');
                $answerQuestions[$question->id] = $answers;
                $question->answersList = $answers;
            }
            return response()->json([
                'questionnaire' => $questionnaire,
                'questions' => $questions,
            ], 200);
        } else {
            return response()->json([
                'error' => 'missing data generic error',
            ], 200);
        }
    }
}
