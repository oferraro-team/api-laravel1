<?php

namespace App\Http\Controllers;

use App\answer;
use App\client_answer;
use App\question;
use App\questionnaire;
use Illuminate\Http\Request;

class ClientAnswerController extends Controller
{
    public function add(Request $request) {
        $request->validate([
            'answer_id' => 'required|integer',
            'client_id' => 'required|integer',
            'value' => 'required'
        ]);
        $answer = answer::find($request->get('answer_id'));
        if ($answer) {
            $clientAnswer = new client_answer;
            $clientAnswer->client_id = $request->get('client_id');
            $clientAnswer->answer_id = $answer->id;
            $clientAnswer->value = $request->get('value');
            if ($clientAnswer->save()) {
                return response()->json([
                    'response' => 'ok',
                    'clientAnswer' => $clientAnswer // is it required to return this?
                ], 200);
            } else { // Todo: use the generic returnError function from the helper
                return response()->json([
                    'error' => 'generic error',
                ], 400);
            }
        } else {
            return response()->json([
                'error' => 'parameters error',
            ], 400);
        }
    }

    public function clientAnswers($clientId, $questionnaireId) {
        $questionnaire = questionnaire::find($questionnaireId);
        $questions = question::where('questionnaire_id', $questionnaire->id)->get();
        if ($questionnaire && $questions) {
            foreach ($questions as $question) {
                $answers = answer::where('question_id', $question->id)->get();
                foreach ($answers as $answer) {
                    $clientAnswers[$answer->id][] = client_answer::where([
                        'client_id' => $clientId,
                        'answer_id' => $answer->id
                    ])->get();
                }
            }
            return response()->json([
                'questionnaire' => $questionnaire,
                'questions' => $questions,
                'answers' => $answers,
                'client_answers' => $clientAnswers
            ], 200);
        } else {
            return response()->json([
                'error' => 'parameters error',
            ], 400);
        }
    }
}
