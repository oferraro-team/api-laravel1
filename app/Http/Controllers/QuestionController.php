<?php

namespace App\Http\Controllers;

use App\questionnaire;
use App\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function add(Request $request) {
        $request->validate([
            'questionnaire_id' => 'required|integer',
            'page_number' => 'required|integer',
            'order' => 'required|integer',
            'type' => 'required|integer',
            'question' => 'required',
            'required' => 'required|integer',
        ]); // first validations (fields exist)

        $questionnaire = Questionnaire::find($request->get('questionnaire_id'));
        if (($request->get('required') == Question::REQUIRED_YES
            || $request->get('required') == Question::REQUIRED_NO)
            && $questionnaire
            && ($request->get('type') == Question::TYPE_SINGLE_SELECT
                || $request->get('type') == Question::TYPE_MULTI_SELECT
                || $request->get('type') == Question::TYPE_SLIDER
                || $request->get('type') == Question::TYPE_TEXT)
        ) {
            $question = new Question;
            $question->questionnaire_id = $request->get('questionnaire_id');
            $question->page_number = $request->get('page_number');
            $question->order = $request->get('order');
            $question->type = $request->get('type');
            $question->question = $request->get('question');
            $question->required = $request->get('required');
            if ($question->save()) {
                return response()->json([
                    'question' => $question,
                ], 200);
            }
            // TODO: Check if question with all these parameters already exist before save it
        } else {
            return response()->json([
                'error' => 'parameters error',
            ], 400);
        }
    }
}
