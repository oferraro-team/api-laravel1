<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class question extends Model
{
    const REQUIRED_YES = 1;
    const REQUIRED_NO = 2;

    const TYPE_SINGLE_SELECT = 3;
    const TYPE_MULTI_SELECT = 4;
    const TYPE_SLIDER = 5;
    const TYPE_TEXT = 6;
}
